<?php

define('IMAGE_FIELD_NAME', 'field_image2');
define('VIDEO_FIELD_NAME', 'field_video');

/**
 * Implements hook_node_load().
 * Load image fields to node.
 */
function imagepoll_node_load($nodes, $types) {
  if (!in_array("poll", $types)) return;

  foreach ($nodes as &$node) {
    foreach($node->choice as &$item) {
      $item['image'] = imagepoll_get_file_by_chid($item['chid']);
    }
  }
}

/**
 * Implements hook_form_FORMID_alter().
 * Override edit form
 */
function imagepoll_form_poll_node_form_alter(&$form, &$form_state, $form_id) {
  // Load file ids from url
  $url_array = explode('/', request_path());
  $node_ids = explode(',', array_pop($url_array));
  //$node_ids =
  if (!empty($node_ids)) {
    $nodes = node_load_multiple($node_ids);
    $new_files = array();
    foreach($nodes as $node) {
      // TODO: set the field name in admin panel
      if (isset($node->field_image2[$node->language][0]['fid'])) {
        $new_files[] = array(
          'nid' => $node->nid,
          'fid' => $node->field_image2[$node->language][0]['fid'],
          'title' => $node->title,
        );
      }
      if (isset($node->field_video[$node->language][0]['fid'])) {
        $new_files[] = array(
          'nid' => $node->nid,
          'fid' => $node->field_video[$node->language][0]['fid'],
          'title' => $node->title,
        );
      }

    }

    // Add extra choice fields
    // TODO: replace weight by delta
    $count_files = count($new_files);
    $delta = $weight = count(element_children($form['choice_wrapper']['choice']));
    for ($delta; $delta < $count_files; $delta++) {
      $key = 'new:' . $delta;
      // Increase the weight of each new choice.
      $weight++;
      $form['choice_wrapper']['choice'][$key] = _poll_choice_form(
        $key, NULL, '', 0, $weight, $count_files
      );
    }
  }

  $form['#title'] = $node->title;
  // Add image fields to choices
  $node = $form['#node'];
  // TODO: change to element_children()
  foreach ($form['choice_wrapper']['choice'] as $key => &$item) {
    if (element_property($key) || !is_array($item)) continue;

    $item['chtext'] = array(
      '#type' => 'tree',
      'text' => $item['chtext'],
    );

    if (isset($node->nid)) {
      // old node
      $choice_id = $item['chid']['#value'];
      $file = $node->choice[$choice_id]['image'];
      $fid = isset($file->fid) ? $file->fid : 0;
      $file_nid = $node->choice[$choice_id]['nid'];
    } elseif (is_array($new_files) && !empty($new_files)) {
      // new node
      $file_info = array_shift($new_files);
      $fid = $file_info['fid'];
      $file_nid = $file_info['nid'];
      $item['chtext']['text']['#value'] = $file_info['title'];
    } else {
      $fid = 0;
      $file_nid = 0;
    }

    $item['chtext']['chimg'] = array(
      '#type' => 'managed_file',
      '#title' => t('File'),
      '#default_value' => $fid,
      '#upload_location' => 'public://imagepoll',
      '#parents' => array('choice', $key, 'image'),
    );

    $item['chtext']['file_nid'] = array(
      '#type' => 'hidden',
      '#value' => $file_nid,
      '#parents' => array('choice', $key, 'file_nid'),
    );
  }

  unset($form['choice_wrapper']['poll_more']);
}


/**
 * Implements hook_form_FORMID_alter().
 * TODO: create theme functions for html code.
 */
function imagepoll_form_poll_view_voting_alter(&$form, &$form_state, $form_id) {
  if (isset($form['choice'])) {
    array_unshift($form['#validate'], 'imagepoll_voting_validate');
    $html = '<div class="imagepoll-view-voting">';

    //$node = $form['#node'];
    // Process element for theme functions
    $form['choice'] += array(
      '#parents' => array(),
      '#attributes' => array(),
    );
    $form_radios = form_process_radios($form['choice']);

    foreach(element_children($form_radios) as $key) {
      $element = $form_radios[$key];
      $html_element = '';
      $choice_id = $key;
      if (is_numeric($choice_id) && $row = imagepoll_get_row_by_chid($choice_id)) {
        $file = file_load($row['fid']);
        $html_element .= '<label>';
        $element += array(
          '#name' => 'choice',
          '#value' => '-1', // TODO: check current value
        );
        $html_element .= theme('radio', array('element' => $element));

        switch($file->type) {
          case 'image':
            $path = image_style_url('medium', $file->uri);
            $html_element .= theme('image', array('path' => $path));
            break;
          case 'video':
            $file_node = node_load($row['file_nid']);
            if (isset($file_node->{VIDEO_FIELD_NAME}[$file_node->language][0])) {
              $html_element .= theme('video_formatter_player', array(
                'item' => $file_node->{VIDEO_FIELD_NAME}[$file_node->language][0],
                'player_dimensions' => '220x165',
              ));
            }
            break;
          default:
            $html_element .= '';
        }
        $html_element .= '</label>';
      }
      $html .= $html_element;
    }

    $html .= '</div>';
    $form['choice'] = array(
      '#type' => 'markup',
      '#markup' => $html,
    );
  }
}

/**
 * Add choice value in form_state
 * TODO: find out why image choice is not set in $form_state
 * @param $form
 * @param $form_state
 */
function imagepoll_voting_validate($form, &$form_state) {
  if (empty($form_state['values']['choice']) &&
    !empty($_REQUEST['choice']) && is_numeric($_REQUEST['choice'])) {
    $form_state['values']['choice'] = $_REQUEST['choice'];
  }
}

/**
 * Implements hook_node_insert().
 */
function imagepoll_node_insert($node) {
  imagepoll_node_save($node);
}

/**
 * Implements hook_node_update().
 */
function imagepoll_node_update($node) {
  imagepoll_node_save($node);
}

function imagepoll_node_save($node) {
  if ($node->type != 'poll') return;

  $new_images = array();
  foreach ($node->choice as $choice) {
    if ($choice['image']) {
      $new_images[$choice['chid']] = $choice['image'];
      $files_nid[$choice['chid']] = $choice['file_nid'];
    }
  }

  $old_images = array();
  if (isset($node->original)) {
    foreach ($node->original->choice as $choice) {
      $fid = false;
      if (is_numeric($choice['image'])) {
        // New nodes
        $fid = $choice['image'];
      } elseif (($choice['image'] instanceof stdClass) && !empty($choice['image']->fid)) {
        // Old nodes
        $fid = $choice['image']->fid;
      }
      if (is_numeric($fid)) {
        $old_images[$choice['chid']] = $fid;
      }
    }
  }

  // Delete images, that existed in the old node, but do not exist in the new node
  foreach ($old_images as $choice_id => $fid) {
    if (!isset($new_images[$choice_id])) {
      imagepoll_image_delete($choice_id);
    }
  }

  foreach ($new_images as $choice_id => $fid) {
    // Delete images, that has changed in the new node
    if (isset($old_images[$choice_id]) && $old_images[$choice_id] != $fid) {
      imagepoll_image_delete($choice_id);
    }

    // Add images, which were not in the old node
    if (!isset($old_images[$choice_id]) || $old_images[$choice_id] != $fid) {
      imagepoll_image_add($node->nid, $choice_id, $fid, $files_nid[$choice_id]);
    }
  }
}

/**
 * Change file status and add relation.
 */
function imagepoll_image_add($nid, $chid, $fid, $file_nid) {
  if (!$fid || !$chid || !$file_nid) {
    return false;
  }

  db_insert('poll_image')
    ->fields(array(
      'chid' => $chid,
      'fid' => $fid,
      'file_nid' => $file_nid,
    ))
    ->execute();

  $file = file_load($fid);
//  $file->status = FILE_STATUS_PERMANENT;
//  file_save($file);
  file_usage_add($file, 'imagepoll', 'node', $nid);
}

/**
 * Delete image file and relation by choice id.
 */
function imagepoll_image_delete($choice_id) {
  $fid = imagepoll_get_fid_by_chid($choice_id);
  if ($fid) {
    // Delete file
    $file = file_load($fid);
    if ($file instanceof stdClass) {
      file_usage_delete($file, 'imagepoll');
      file_delete($file);
    }

    // Delete relation
    db_delete('poll_image')
      ->condition('chid', $choice_id)
      ->execute();
  }
}

function imagepoll_get_fid_by_chid($choice_id) {
  if (!is_numeric($choice_id)) return false;

  $fid = db_select('poll_image', 'pi')
    ->fields('pi', array('fid'))
    ->condition('chid', $choice_id)
    ->execute()
    ->fetchField();

  if (!is_numeric($fid)) {
    $fid = false;
  }
  return $fid;
}

function imagepoll_get_row_by_chid($choice_id) {
  if (!is_numeric($choice_id)) return false;

  $row = db_select('poll_image', 'pi')
    ->fields('pi', array('chid', 'fid', 'file_nid'))
    ->condition('chid', $choice_id)
    ->execute()
    ->fetchAssoc();

  if (!is_numeric($row['fid'])) {
    return false;
  }
  return $row;
}

/**
 * @param $choice_id - load file from db by choice id
 * @return bool|mixed
 */
function imagepoll_get_file_by_chid($choice_id) {
  $fid = imagepoll_get_fid_by_chid($choice_id);

  $file = false;
  if (is_numeric($fid)) {
    $file = file_load($fid);
  }
  return $file;
}

/**
 * Implements hook_preprocess_node().
 * Add css.
 * @param $node
 */
function imagepoll_preprocess_node($node) {
  if ($node['type'] == 'poll') {
    drupal_add_css(drupal_get_path('module', 'imagepoll') . '/imagepoll.css');
    drupal_add_js(drupal_get_path('module', 'imagepoll') . '/imagepoll.js');
  }
}